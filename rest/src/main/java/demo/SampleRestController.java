package demo;


import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.RestController;

import java.util.List;
import demo.domain.User;
import demo.service.OrgStructureService;


@RestController
@RequestMapping("/myapi")
public class SampleRestController {

    @Autowired
    OrgStructureService orgStructureService;

    @RequestMapping("/users")
    List<User> home() {
        return orgStructureService.findAllUsers();
    }




}
