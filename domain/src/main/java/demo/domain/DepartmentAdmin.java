package demo.domain;

import org.springframework.data.rest.core.config.Projection;

import javax.persistence.Column;


@Projection(types=Department.class)
public interface DepartmentAdmin {


    public String getName();

    public String getRegNumber();

    public String getOwner();

    public String getAddress();



}
