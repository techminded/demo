package demo.domain;

import java.util.List;

import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;

public class MyUserRepositoryImpl implements MyUserRepository {
    @PersistenceContext
    EntityManager em;

    public List<User> findByAge(Integer age) {
        List<User> users = (List<User>) em.createQuery("SELECT User from User u where u.age = ?1").getResultList();
        return users;
    }

}
