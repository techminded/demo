package demo.domain;


import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.rest.core.annotation.RepositoryRestResource;

import java.util.List;

@RepositoryRestResource(collectionResourceRel = "api_departments", path = "api_departments", excerptProjection = DepartmentPub.class)
public interface DepartmentRepository extends JpaRepository<Department, Long> {

    public List<Department> findAll();

}
