package demo.domain;

import java.util.List;

public interface MyUserRepository {
    List<User> findByAge(Integer age);
}
