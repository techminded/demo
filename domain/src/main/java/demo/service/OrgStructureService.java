package demo.service;


import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.scheduling.annotation.Scheduled;
import org.springframework.stereotype.Service;

import java.util.List;

import javax.annotation.PostConstruct;

import demo.domain.Department;
import demo.domain.DepartmentRepository;
import demo.domain.User;
import demo.domain.UserRepository;

@Service
public class OrgStructureService {
    @Autowired
    UserRepository userRepository;

    @Autowired
    DepartmentRepository departmentRepository;

    @PostConstruct
    public void createData() {
        this.departmentRepository.save(new Department("main"));
        Department department = this.departmentRepository.save(new Department("it"));

        User user = new User();
        user.setName("john doe");
        user.setAge(16);
        user.setDepartment(department);
        userRepository.save(user);
    }

    public String buildUsersString(String departmentName) {
        String userString = "";
        List<User> users = this.userRepository.findAll();
        for (User item: users) {
            if (departmentName != null && departmentName.equals(item.getDepartment().getName())) {
                userString += item.toString();
            }
        }
        return userString;
    }

    public String buildUsersString(Integer age) {
        String userString = "";
        List<User> users = this.userRepository.findByAge(age);
        for (User item: users) {
            userString += item.toString();
        }
        return userString;
    }

    public List<User> findAllUsers() {
        return userRepository.findAll();
    }


}
