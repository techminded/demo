package demo;


import org.springframework.scheduling.annotation.Async;
import org.springframework.scheduling.annotation.Scheduled;
import org.springframework.stereotype.Service;

import java.util.Random;
import java.util.concurrent.CompletableFuture;
import java.util.concurrent.ThreadLocalRandom;
import java.util.logging.Logger;

@Service
public class SumService {


    final Logger logger = Logger.getLogger(SumService.class.getName());


    public int sum(int a, int b) {
        return a + b;
    }

    @Async
    public CompletableFuture<Integer> calcAsync(int a, int b) {
        logger.info("Started counting sum for a="+a+" + b=" + b);
        try {
            int randomNum = ThreadLocalRandom.current().nextInt(5, 10 + 1);
            Thread.sleep(1000 * randomNum);
            logger.info("Finished counting sum for a="+a+" + b=" + b + ", result=" + (a + b));
            return CompletableFuture.completedFuture(a + b);
        } catch (InterruptedException e) {
            e.printStackTrace();
        }
        return CompletableFuture.completedFuture(0);
    }
}
