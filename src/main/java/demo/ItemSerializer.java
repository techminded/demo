package demo;

import com.fasterxml.jackson.core.JsonGenerator;
import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.SerializerProvider;
import com.fasterxml.jackson.databind.ser.std.StdSerializer;

import java.io.IOException;

import demo.dto.DataItemTO;

public class ItemSerializer extends StdSerializer<DataItemTO> {
     
    public ItemSerializer() {
        this(null);
    }
   
    public ItemSerializer(Class<DataItemTO> t) {
        super(t);
    }
 
    @Override
    public void serialize(
            DataItemTO value, JsonGenerator jgen, SerializerProvider provider)
      throws IOException, JsonProcessingException {
/*
        jgen.writeStartObject();
        jgen.writeNumberField("id", value.id);
        jgen.writeStringField("itemName", value.itemName);
        jgen.writeNumberField("owner", value.owner.id);
        jgen.writeEndObject();*/
    }
}