package demo;


import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.SessionAttributes;

import java.util.ArrayList;
import java.util.List;
import java.util.logging.Logger;

import javax.servlet.http.HttpServletRequest;


@RequestMapping("/myhome")
@Controller
public class MyHomeController {

    @Autowired
    private HttpServletRequest request;

    final Logger logger = Logger.getLogger(MyHomeController.class.getName());


    @RequestMapping("/")
    String home() {

        return "myhome";
    }

    @RequestMapping("/test")
    String test(Model model) {
        model.addAttribute("foo", "bar");
        List<Order> orders = new ArrayList<Order>() {{
            add(new Order("order 1", 100));
            add(new Order("order 2", 200));
            add(new Order("order 3", 1020));
            add(new Order("order 4", 100));
        }};
        model.addAttribute("orders", orders);
        return "test";
    }



}
