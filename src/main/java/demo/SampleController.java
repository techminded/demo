package demo;



import com.fasterxml.jackson.databind.JsonNode;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.fasterxml.jackson.databind.module.SimpleModule;
import com.jayway.jsonpath.JsonPath;
import com.sun.javafx.binding.Logging;

import org.activiti.engine.RepositoryService;
import org.activiti.engine.RuntimeService;
import org.dozer.DozerBeanMapper;
import org.dozer.Mapper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.context.annotation.Primary;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.projection.ProjectionFactory;
import org.springframework.expression.Expression;
import org.springframework.expression.ExpressionParser;
import org.springframework.expression.spel.standard.SpelExpressionParser;
import org.springframework.expression.spel.support.StandardEvaluationContext;
import org.springframework.scheduling.concurrent.ThreadPoolTaskExecutor;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.*;

import java.util.HashMap;
import java.util.HashSet;
import java.util.List;
import java.util.Map;
import java.util.Set;
import java.util.concurrent.Callable;
import java.util.concurrent.CompletableFuture;
import java.util.concurrent.Future;
import java.util.concurrent.TimeUnit;
import java.util.logging.Logger;

import javax.annotation.PostConstruct;
import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import javax.websocket.server.PathParam;

import demo.domain.Department;
import demo.domain.DepartmentAdm;
import demo.domain.DepartmentAdmin;
import demo.domain.DepartmentRepository;
import demo.domain.User;
import demo.domain.UserRepository;
import demo.dto.DataItem2TO;
import demo.dto.DataItemTO;
import demo.service.OrgStructureService;
import okhttp3.OkHttpClient;
import okhttp3.Request;
import okhttp3.Response;

import org.activiti.engine.TaskService;
import org.activiti.engine.runtime.ProcessInstance;


@RestController
@RequestMapping("/sample")
public class SampleController {


    final Logger logger = Logger.getLogger(SampleController.class.getName());

    @Autowired
    OrgStructureService orgStructureService;

    @Value("${foo.bar:'default'}")
    String baz;

    @Autowired
    MyConfiguration myConfiguration;

    @Autowired
    SumService sumService;

    @Autowired
    ProjectionFactory projectionFactory;

    @Autowired
    DepartmentRepository departmentRepository;

    @Autowired
    RepositoryService repositoryService;

    @Autowired
    RuntimeService runtimeService;

    @Autowired
    TaskService taskService;

    ExpressionParser parser = new SpelExpressionParser();


    @RequestMapping("/test/{name}")
    String home(@PathVariable("name") String name) throws Exception {
/*        orgStructureService.findAllUsers().stream().map((User user) -> {
            user.getAge();
        }).filter().to;*/

        return "yo";
    }

    @RequestMapping("/test")
    String test() throws Exception {

        String jsonString = "{\"firstName\": null, \"lastName\": \"Doe\"}";

        ObjectMapper mapper = new ObjectMapper();
        SimpleModule module = new SimpleModule();
        module.addDeserializer(DataItemTO.class, new ItemDeserializer());
        mapper.registerModule(module);

        Mapper beanMapper = new DozerBeanMapper();

        DataItemTO item = mapper.readValue(jsonString, DataItemTO.class);
        item.firstName = "Ivan";

        DataItem2TO item2 = new DataItem2TO();
        beanMapper.map(item, item2);

        item2.age = 18;
        StandardEvaluationContext context = new StandardEvaluationContext();
        context.setVariable("item2", item2);
        Expression exp = parser.parseExpression("#item2.firstName + ' ' + #item2.age");

        String result = (String) exp.getValue(context, item2);

        String firstName = JsonPath.read(jsonString, "$.lastName");

        String newJsonString = mapper.writeValueAsString(item2);
        return newJsonString;
    }


    @PreAuthorize(value = "hasRole('ROLE_USER')")
    @RequestMapping(value = "/departments")
    public Page<?> listDepartments(Pageable pageable) {

        return departmentRepository.findAll(pageable).//
                map(department -> projectionFactory.createProjection(DepartmentAdmin.class, department));
    }

    @RequestMapping(value = "/process")
    public String startProcess() {
        Map<String, Object> variables = new HashMap<String, Object>();
        variables.put("applicantName", "John Doe");
        variables.put("email", "john.doe@activiti.com");
        variables.put("phoneNumber", "123456789");
        runtimeService.startProcessInstanceByKey("hireProcess", variables);
        return "hello";
    }


}
