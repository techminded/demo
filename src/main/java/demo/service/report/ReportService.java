package demo.service.report;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import demo.SumService;

@Service
public class ReportService {

    @Autowired
    SumService sumService;

    public String sumReport(int a, int b) {
        return String.valueOf(sumService.sum(a, b));
    }
}
