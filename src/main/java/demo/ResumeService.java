package demo;

import org.springframework.stereotype.Service;

import java.util.logging.Logger;

@Service
public class ResumeService {

    final Logger logger = Logger.getLogger(SampleController.class.getName());

    public void storeResume() {
        logger.info("ResumeService::storeResume");
    }
}
