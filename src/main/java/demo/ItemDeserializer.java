package demo;

import com.fasterxml.jackson.core.JsonParser;
import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.DeserializationContext;
import com.fasterxml.jackson.databind.JsonNode;
import com.fasterxml.jackson.databind.deser.std.StdDeserializer;

import java.io.IOException;

import demo.dto.DataItemTO;

public class ItemDeserializer extends StdDeserializer<DataItemTO> {
 
    public ItemDeserializer() { 
        this(null); 
    } 
 
    public ItemDeserializer(Class<?> vc) { 
        super(vc); 
    }
 
    @Override
    public DataItemTO deserialize(JsonParser jp, DeserializationContext ctxt)
      throws IOException, JsonProcessingException {
        JsonNode node = jp.getCodec().readTree(jp);
        String firstName = node.get("firstName").asText("no data");
        DataItemTO dataItemTO = new DataItemTO();
        dataItemTO.firstName = firstName;

        return dataItemTO;
    }
}